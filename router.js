Bloggy.Router.map(function() {
	this.resource('posts',{path: "/"});
	this.resource('post', {path: ":post_id"});
	this.resource('about');
	this.resource('contact');
	this.resource('login');
	this.resource('register');
});

