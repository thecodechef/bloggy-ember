var Helper = Ember.Handlebars.helper;

Helper('formatDate', function(date) {
	return moment(date).fromNow();
});

var showdown = Showdown.converter();

Helper('showDown', function(input) {
	return new Handlebars.SafeString(showdown.makeHtml(input));
});