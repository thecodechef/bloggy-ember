Bloggy.PostsController = Ember.Controller.extend({
	posts: [
		
		{
			id: '1',
			title: "Ember.js is Awesome",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		},
		{
			id: '2',
			title: "Getting Started with Ember.js",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		},
		{
			id: '3',
			title: "Hands on Ember.js",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		},
		{
			id: '4',
			title: "Ember.js 101",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		},
		{
			id: '5',
			title: "Ember.js Tips & Tricks",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		},
		{
			id: '6',
			title: "Ember Meetups",
			author: "Jeremy Bolding",
			date: "2014-11-19",
			avatar: "http://placehold.it/100x100",
			postImg: "http://placehold.it/500x250"
		}

	]
});

